const uri = 'https://ftx.com'
const basepath = '/api'
var user = PropertiesService.getScriptProperties().getProperties();

var api_balances, api_market, api_fills;

//想取得獲利相關資訊，請執行 profit_log
function profit_log() {
    if (api_balances == null)
        api_balances = APIGetBalances();

    //coin
    var base_total = 0;
    var base_usdvalue = 0;
    var quote_total = 0;
    var quote_usdvalue = 0;

    var result = GetBalances(user.baseCurrency);
    if (result != null) {
        base_total = result.total;
        base_usdvalue = result.usdValue;
    }

    result = GetBalances(user.quoteCurrency);
    if (result != null) {
        quote_total = result.total;
        quote_usdvalue = result.usdValue;
    }

    //check_profit();

    var log;
    log = "\n";
    //log += "~ AVAX 網格機器人 v04 ~";
    log += "~ " + user.botName + " ~";
    log += "\n";
    log += "獲利: " + Math.round(user.profit * 1000000) / 1000000 + " " + user.quoteCurrency;
    log += ", 價值：" + Math.round(user.profit * quote_usdvalue / quote_total * 100) / 100 + " USD";
    log += "\n";
    log += "網格大小: " + user.grid_pct + " %";
    log += "\n";
    log += "網格交易量: " + user.grid_volume;
    log += "\n";
    //log += "買入次數: " + user.fill_buy;
    //log += "\n";
    log += "買入顆數: " + Math.round(user.size_buy * 1000000) / 1000000;
    log += "\n";
    //log += "賣出次數: " + user.fill_sell;
    //log += "\n";
    log += "賣出顆數: " + Math.round(user.size_sell * 1000000) / 1000000;
    log += "\n";
    log += user.baseCurrency + " 餘額: " + Math.round(base_total * 1000000) / 1000000;
    log += "\n";
    log += user.baseCurrency + " 價值: " + Math.round(base_usdvalue * 100) / 100;
    log += "\n";
    log += user.quoteCurrency + " 餘額: " + Math.round(quote_total * 1000000) / 1000000;
    log += "\n";
    log += user.quoteCurrency + " 價值: " + Math.round(quote_usdvalue * 100) / 100;
    log += "\n";
    log += "帳戶價值: " + Math.round((quote_usdvalue + base_usdvalue) * 100) / 100;
    log += "\n";

    Logger.log(log);

    return log;
}

function check_profit() {
    var profit = 0;
    var pct = user.grid_pct / 100;
    var p = 0;
    var latest_fill_id = Number(user.latest_fill_id);
    var profit = Number(user.profit);
    var size_buy = Number(user.size_buy);
    var size_sell = Number(user.size_sell);
    var fill_buy = Number(user.fill_buy);
    var fill_sell = Number(user.fill_sell);

    if (api_fills == null)
        api_fills = APIGetFills(user.baseCurrency + "/" + user.quoteCurrency);

    if (api_fills.length <= 0) {
        Logger.log("no fills");
        return;
    }

    Logger.log("check profit after id: " + latest_fill_id);

    var idx;
    for (idx = 0; idx < api_fills.length; idx++) {
        if (api_fills[idx].id == latest_fill_id) {
            Logger.log("check profit done");
            //return profit;
            break;
        }

        if (api_fills[idx].side == "buy") {
            //Logger.log("ignore buying");
            fill_buy += 1;
            size_buy += api_fills[idx].size;
            continue;
        }

        fill_sell += 1;
        size_sell += api_fills[idx].size;
        p = (api_fills[idx].price / (1 + pct) * api_fills[idx].size * pct - api_fills[idx].fee);
        profit += p;

        var log = "交易成功 id: " + api_fills[idx].id;
        log += "," + api_fills[idx].side;
        log += ",獲利: " + p;
        log += ",價位: " + api_fills[idx].price;
        log += ",量: " + api_fills[idx].size;
        log += ",手續費: " + api_fills[idx].fee;
        Logger.log(log);

    }

    user.latest_fill_id = api_fills[0].id;
    user.profit = profit;
    user.size_buy = size_buy;
    user.size_sell = size_sell;
    user.fill_sell = fill_sell;
    user.fill_buy = fill_buy;

    profit_log();

    PropertiesService.getScriptProperties().setProperties(user);
}

function TriggerMe() {
    var baseCurrency = user.baseCurrency;
    var quoteCurrency = user.quoteCurrency;
    var market_name = baseCurrency + "/" + quoteCurrency;
    var grid_pct = user.grid_pct / 100;
    var grid_volume = Number(user.grid_volume);
    var grid_number = Number(user.grid_number);
    var first_time = Number(user.first_time);
    var post_only = Number(user.post_only);
    var last_orders = Number(user.last_orders);
    var price;
    var orders;
    var market;

    orders = APIGetOrders(market_name);
    Logger.log("目前有 " + orders.length + " 筆訂單");

    if (first_time == 1) {
        Logger.log("First Time: " + first_time);

        market = APIGetSingleMarket(market_name);
        //Logger.log(market);
        price = market.price;

        user.first_time = 0;
        PropertiesService.getScriptProperties().setProperty('first_time', user.first_time);

    } else {
        if (last_orders > 0 && orders.length == last_orders) {
            Logger.log("掛單數量無變化，結束此次流程");
            return;
        }

        if (api_fills == null)
            api_fills = APIGetFills(market_name);

        if (api_fills.length > 0) {
            fill = GetLatestFills(market_name);

            if (fill.liquidity == "taker") {
                var history = APIGetOrderHistory(market_name).find((v) => v.id === fill.orderId)
                if (history && history.price) {
                    price = history.price;
                    Logger.log("history.price: " + history.price);
                } else
                    price = fill.price;
            } else
                price = fill.price;

            check_profit();
        } else {
            market = APIGetSingleMarket(market_name);
            price = market.price;
            Logger.log("無成交記錄，以市價 " + price + " 為中心價");
        }
    }

    if (orders.length > 0)
        CancelAllOrders(market_name);

    GridsPlaceOrders(market_name, Number(price), Number(grid_pct), Number(grid_number), Number(grid_volume), Number(post_only));

}

function GetLatestFills(market_name) {
    if (api_fills == null)
        api_fills = APIGetFills(market_name);

    if (api_fills.length <= 0) {
        Logger.log("No Fills for " + market_name);
        return null;
    }

    return api_fills[0];
}

function GridsPlaceOrders(market_name, start_price, grid_pct, grid_num, grid_vol, post_only) {

    var x = 1 + grid_pct;
    var info;
    var enough_baseCurrency = false;
    var enough_quoteCurrency = false;
    var orders = 0;

    if (user.post_only == 1)
        post_only = true;
    else
        post_only = false;

    api_balances = APIGetBalances();

    var result = GetBalances(user.baseCurrency);
    if (result != null && result.free > grid_num * grid_vol)
        enough_baseCurrency = true;
    else if (result != null) {
        var msg = user.baseCurrency + " 餘額不足 !!!";
        msg += "\n";
        msg += " 目前可用餘額： " + result.free + ", 開單共需要： " + (grid_num * grid_vol);
        msg += "\n";

        Logger.log(msg);
        LineNotify(msg);
    } else {
        var msg = user.baseCurrency + " 無法取得餘額";

        Logger.log(msg);
        LineNotify(msg);
    }


    var total = 0;
    var p = start_price;
    for (var i = 0; i < grid_num; i++) {
        p = p / (1 + grid_pct);
        total += (p * grid_vol);
    }
    result = GetBalances(user.quoteCurrency);
    if (result != null && result.free > total)
        enough_quoteCurrency = true;
    else if (result != null) {
        var msg = user.quoteCurrency + " 餘額不足 !!!";
        msg += "\n";
        msg += " 目前可用餘額： " + result.free + ", 開單共需要： " + total;
        msg += "\n";

        Logger.log(msg);
        LineNotify(msg);
    } else {
        var msg = user.quoteCurrency + " 無法取得餘額";

        Logger.log(msg);
        LineNotify(msg);
    }

    Logger.log("佈置網格訂單，中心價： " + start_price);

    for (var i = 0; i < grid_num; i++) {
        Logger.log("第" + (i + 1) + "組訂單: ");

        if (enough_quoteCurrency == true) {
            info = "買入價：" + start_price / x;
            info += ", 數量：" + grid_vol + "顆";
            info += ", postOnly：" + post_only;
            Logger.log(info);

            result = APIPlaceOrder(market_name, "buy", start_price / x, "limit", grid_vol, false, false, post_only);
            orders++;
            //Logger.log(result);
        } else {
            Logger.log(user.quoteCurrency + " 餘額不足 !!!");
            Logger.log(" 目前餘額： " + user.quoteCurrency);
        }

        if (enough_baseCurrency == true) {
            info = "賣出價：" + start_price * x;
            info += ", 數量：" + grid_vol + "顆";
            info += ", postOnly：" + post_only;
            Logger.log(info);

            result = APIPlaceOrder(market_name, "sell", start_price * x, "limit", grid_vol, false, false, post_only);
            orders++;
            //Logger.log(result);
        } else {
            Logger.log(user.baseCurrency + " 餘額不足 !!!");
            Logger.log(" 目前餘額： " + user.quoteCurrency);
        }

        x *= (1 + grid_pct);
    }

    user.last_orders = orders;
    PropertiesService.getScriptProperties().setProperty('last_orders', user.last_orders);

    return orders;
}

function any_new_fills(market, latest_fill_id) {

    if (api_fills == null)
        api_fills = APIGetFills(market);

    if (api_fills.length > 0 && api_fills[0].id != latest_fill_id)
        return true;
    else if (api_fills.length == 0)
        return true;
    else
        return false;
}

function CancelAllOrders(market_name) {
    var result = APICancelAllOrders(market_name);
    Logger.log(result);
}

function GetBalances(coin) {
    var target = api_balances.find((v) => v.coin === coin)

    return target;
}

function Line_profit_log() {

    check_profit();
    var log = profit_log();

    LineNotify(log);
}

function LineNotify(message) {
    var options = {
        'method': 'post',
        'payload': { 'message': message },
        'headers': { 'Authorization': 'Bearer ' + user.lineToken }
    };
    UrlFetchApp.fetch('https://notify-api.line.me/api/notify', options);
}
