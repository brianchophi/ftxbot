package gbot

import (
	"fmt"
	"ftxbot/ftxexchange/ftx"
	"ftxbot/ftxexchange/ftx/structs"
	"time"
)

func NewGbot(set *SettingsInfo) *Gbot {
	g := new(Gbot)
	g.Client = ftx.New(set.Apikey, set.Apisecret, set.Subaccount_name)
	g.Settings = set
	g.Market_name = g.Settings.Market_Name

	g.OpenOrders = make([]structs.Order, 0)
	g.OrderHistoryMap = make(map[int64]structs.Order)
	g.MarketMap = make(map[string]structs.MargetResult)

	g.StartAt = time.Now().Unix()
	g.FillCheckAt = time.Now().Unix()

	return g
}

func (g *Gbot) GridsRun() {
	fmt.Printf("%v,初始化完成\n", g.Settings.BotName)
	fmt.Printf("market = %v, pct = %v, vol = %v\n", g.Settings.Market_Name, g.Settings.Grid_pct, g.Settings.Grid_volume)

	g.Client.CancelTheMarketOrders(g.Market_name)
	g.UpdateMarket()

	market := g.MarketMap[g.Market_name]

	// 以最新價格 market.Last 部單
	g.GridsPlaceOrders(market.Last)

	tick := time.NewTicker(2 * time.Second)
	for range tick.C {
		g.CheckFilled()
	}
}

func (g *Gbot) GridsPlaceOrders(start_price float64) {
	x := 1 + g.Settings.Grid_pct
	for i := 0; i < g.Settings.Grid_number; i++ {
		g.PlaceOrder("buy", start_price/x)
		g.PlaceOrder("sell", start_price*x)
		x *= (1 + g.Settings.Grid_pct)
	}
}
func (g *Gbot) PlaceOrder(side string, price float64) {
	g.Client.PlaceOrder(
		g.Market_name,
		side,
		price,
		"limit",
		g.Settings.Grid_volume,
		false,
		false,
		g.Settings.Post_only)
}

func (g *Gbot) CloseOrder(orderId int64) {
	g.Client.CancelOrder(orderId)
}

func (g *Gbot) CheckProfit() {
	if len(g.Fills) == 0 {
		return
	}
	buyList := []float64{}
	sellList := []float64{}
	for _, f := range g.Fills {
		if f.Id <= g.LastFillId {
			continue
		}
		if f.Side == "buy" {
			buyList = append(buyList, f.Price*f.Size-f.Fee)
		} else {
			sellList = append(sellList, f.Price*f.Size-f.Fee)
		}
	}

	fmt.Println(buyList)
	fmt.Println(sellList)
}

func (g *Gbot) CheckFilled() {
	g.UpdateOpenOrder()

	if len(g.OpenOrders) == g.LastOrderNum {
		return
	}

	g.UpdateFills()
	g.UpdateMarket()

	start_price := g.MarketMap[g.Market_name].Price
	if len(g.Fills) > 0 {
		lastFill := g.Fills[0]
		start_price = lastFill.Price
		if lastFill.Liquidity == "taker" {
			historyOrder, ok := g.OrderHistoryMap[lastFill.OrderId]
			if ok && historyOrder.Price > 0 {
				start_price = historyOrder.Price
			}
		}
	}

	if len(g.OpenOrders) > 0 {
		g.Client.CancelTheMarketOrders(g.Market_name)
	}
	g.GridsPlaceOrders(start_price)
	g.UpdateOpenOrder()
	g.LastOrderNum = len(g.OpenOrders)
}

func (g *Gbot) GetLastFill() structs.Fill {
	last, err := g.Client.GetLastFill(g.Market_name)
	if err != nil || !last.Success {
		return structs.Fill{}
	}
	return last.Result[0]
}
