package gbot

// 更新最近1000筆OrderHistory
func (g *Gbot) UpdateOrderHistory() {
	his, err := g.Client.GetOrderHistory(g.Market_name, 0, 0, 1000)
	if err != nil || !his.Success {
		return
	}
	for _, h := range his.Result {
		g.OrderHistoryMap[h.ID] = h
	}
}

// 更新OpenOrder
func (g *Gbot) UpdateOpenOrder() {
	open, err := g.Client.GetOpenOrders(g.Market_name)
	if err != nil || !open.Success {
		return
	}
	g.OpenOrders = open.Result
}

// 更新市場價格
func (g *Gbot) UpdateMarket() {
	market, err := g.Client.GetMarkets()
	if err != nil || !market.Success {
		return
	}
	for _, m := range market.Result {
		g.MarketMap[m.Name] = m
	}
}

// 更新成交單
func (g *Gbot) UpdateFills() {
	fills, err := g.Client.GetFills(g.Market_name, g.StartAt)
	if err != nil || !fills.Success {
		return
	}
	g.Fills = fills.Result

}
