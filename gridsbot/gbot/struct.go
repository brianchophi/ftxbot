package gbot

import (
	"ftxbot/ftxexchange/ftx"
	"ftxbot/ftxexchange/ftx/structs"
)

type SettingsInfo struct {
	BotName         string  `yaml:"botName"`
	Apikey          string  `yaml:"apikey"`
	Apisecret       string  `yaml:"apisecret"`
	Subaccount_name string  `yaml:"subaccount_name"`
	Market_Name     string  `yaml:"market_name"`
	Grid_pct        float64 `yaml:"grid_pct"`
	Grid_volume     float64 `yaml:"grid_volume"`
	Grid_number     int     `yaml:"grid_number"`
	Post_only       bool    `yaml:"post_only"`
	LineToken       string  `yaml:"lineToken"`
}

type Gbot struct {
	Client      *ftx.FtxClient
	Settings    *SettingsInfo
	Market_name string

	LastOrderNum    int
	OpenOrders      []structs.Order
	OrderHistoryMap map[int64]structs.Order
	MarketMap       map[string]structs.MargetResult

	LastFillId int64

	Fills []structs.Fill

	StartAt     int64
	FillCheckAt int64
}
