// My LuYa Portfolios APP v3.0
// https://docs.google.com/spreadsheets/d/15FibFyNJfWV-IhIGC6NCLrcJu9D6QeYIuH0kAEyRPuw/edit?usp=sharing

// 更新紀錄
// v3.1 新增清除前次帳戶資訊功能
// v3.0 優化試算表排版,與程式碼效能優化
// v2.1 增加一鍵同步
// v2.0 使用FTX api 獲取目前帳戶餘額
// v1.0 使用FTX api 獲取目前市場價格

const uri = 'https://ftx.com'
const basepath = '/api'
const keys = {}
var defaut_account = ''

//以下為使用者參數設定
const conf = {
    usersetting: 'user_setting', // 存放API-Keys的工作表名稱
    autofetch: 'FTX', // 自動更新的工作表名稱
    usdValueTolerence: 0.01, // 美金小於容忍值不顯示
    account_clear_num: 100, // 清除前次帳戶紀錄
};

var auto_fetch_sheet
var user_setting_sheet


// 全更新
function UpdataAll() {
    UpdateInit()
    UpdatePrice()
    UpdataAccount()
    Logger.log('UpdataAll() is done at ' + new Date)
}


// 設定初始化
function UpdateInit() {
    auto_fetch_sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(conf.autofetch);
    user_setting_sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(conf.usersetting);
    auto_fetch_sheet.getRange("B1").setValue(new Date())
    Logger.log('Now is ' + new Date())
}

// 更新追蹤幣價
function UpdatePrice() {
    updatekeys()

    auto_fetch_sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(conf.autofetch);
    // 檢查定位點
    anchor_idx = 1
    columnA = auto_fetch_sheet.getRange("A1:A100").getValues()
    for (i = 0; i < columnA.length; i++) {
        if (columnA[i][0] == "定位點") {
            anchor_idx = i + 1
            break
        }
    }

    // 更新幣價
    markets = APIGetMarkets(defaut_account);
    coins_list = auto_fetch_sheet.getRange("A" + (anchor_idx + 2) + ":" + "A100").getValues()

    coins_prices = []
    for (i = 0; i < coins_list.length; i++) {
        if (coins_list[i].length == 0 || coins_list[i][0].length == 0) {
            break
        }
        m = GetMarket(markets, coins_list[i][0])
        Logger.log(coins_list[i] + " is updated.")
        coins_prices.push([m.price])
    }
    auto_fetch_sheet.getRange("B" + (anchor_idx + 2) + ":" + "B" + (anchor_idx + 2 + coins_prices.length - 1)).setValues(coins_prices)
    Logger.log('UpdatePrice() is done.')
}

// 更新追蹤帳戶
function UpdataAccount() {

    updatekeys()

    auto_fetch_sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(conf.autofetch);
    // 檢查定位點
    anchor_idx = 1
    columnA = auto_fetch_sheet.getRange("D1:D100").getValues()
    for (i = 0; i < columnA.length; i++) {
        if (columnA[i][0] == "定位點") {
            anchor_idx = i + 1
            break
        }
    }


    auto_fetch_sheet.getRange("D" + (anchor_idx + 2) + ":" + "I" + (anchor_idx + 2 + conf.account_clear_num - 1)).clear()


    account_info_list = []
    for (k in keys) {
        balances = APIGetAllBalances(k)
        for (var sub in balances) {
            for (var i = 0; i < balances[sub].length; i++) {
                if (Math.abs(balances[sub][i]["usdValue"]) < conf.usdValueTolerence) {
                    continue
                }
                account_info = []
                account_info.push(
                    k,
                    sub,
                    balances[sub][i]["coin"],
                    balances[sub][i]["total"],
                    balances[sub][i]["free"],
                    balances[sub][i]["usdValue"])
                Logger.log(account_info + " is updated.")
                account_info_list.push(account_info)
            }
        }
    }
    auto_fetch_sheet.getRange("D" + (anchor_idx + 2) + ":" + "I" + (anchor_idx + 2 + account_info_list.length - 1)).setValues(account_info_list)



    // 檢查定位點
    anchor_idx = 1
    columnA = auto_fetch_sheet.getRange("K1:K100").getValues()
    for (i = 0; i < columnA.length; i++) {
        if (columnA[i][0] == "定位點") {
            anchor_idx = i + 1
            break
        }
    }

    auto_fetch_sheet.getRange("K" + (anchor_idx + 2) + ":" + "P" + (anchor_idx + 2 + conf.account_clear_num - 1)).clear()



    sub_info_list = []
    var acc = CommandGet("/account", keys[defaut_account].apikey, keys[defaut_account].apisecret)
    Logger.log(["main", acc.freeCollateral, acc.initialMarginRequirement, acc.openMarginFraction, acc.totalAccountValue, acc.totalPositionSize])
    sub_info_list.push(["main", acc.freeCollateral, acc.initialMarginRequirement, acc.openMarginFraction, acc.totalAccountValue, acc.totalPositionSize])

    var resp = CommandGet("/subaccounts", keys[defaut_account].apikey, keys[defaut_account].apisecret)
    var subs = []
    for (i = 0; i < resp.length; i++) {
        subs.push(resp[i].nickname)
    }
    subs.sort()
    for (i = 0; i < subs.length; i++) {
        var acc = CommandGet("/account", keys[defaut_account].apikey, keys[defaut_account].apisecret, subs[i])
        Logger.log([subs[i], acc.freeCollateral, acc.initialMarginRequirement, acc.openMarginFraction, acc.totalAccountValue, acc.totalPositionSize])
        sub_info_list.push([subs[i], acc.freeCollateral, acc.initialMarginRequirement, acc.openMarginFraction, acc.totalAccountValue, acc.totalPositionSize])
    }

    auto_fetch_sheet.getRange("K" + (anchor_idx + 2) + ":" + "P" + (anchor_idx + 2 + sub_info_list.length - 1)).setValues(sub_info_list)




    Logger.log('UpdataAccount() is done.')
}

// 讀取試算表上的key
function updatekeys() {
    user_setting_sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(conf.usersetting);
    datas = user_setting_sheet.getRange("A2:D100").getValues();
    for (var i = 0; i < datas.length; i++) {
        if (datas[i][2].length == 0) {
            break
        }
        keys[datas[i][1]] = {
            apikey: datas[i][2],
            apisecret: datas[i][3],
        }
    }
    defaut_account = datas[0][1];
}

function GetMarket(markets, coin) {
    var target = markets.find((v) => (v.baseCurrency === coin && v.quoteCurrency === "USD"))
    return target;
}

// 獲取所有市場詳情
function APIGetMarkets(main_account) {
    return AllCommandGet("/markets", keys[main_account].apikey, keys[main_account].apisecret);
}

// 獲取所有帳戶餘額
function APIGetAllBalances(main_account) {
    return AllCommandGet("/wallet/all_balances", keys[main_account].apikey, keys[main_account].apisecret);
}

function CommandGet(cmdpath, apikey, apisecret, subaccount_name) {
    var time_now = String(Date.now());
    var method = 'GET';
    var path = basepath + cmdpath;
    var sign = Utilities.computeHmacSha256Signature(time_now + method + path, apisecret)
        .map(function (chr) { return (chr + 256).toString(16).slice(-2) })
        .join('')

    var header
    if (subaccount_name == null) {
        header = {
            'FTX-KEY': apikey,
            'FTX-TS': time_now,
            'FTX-SIGN': sign,
        };
    } else {
        header = {
            'FTX-KEY': apikey,
            'FTX-TS': time_now,
            'FTX-SIGN': sign,
            'FTX-SUBACCOUNT': subaccount_name,
        };
    }
    var options = {
        'method': method,
        'headers': header
    };
    return JSON.parse(UrlFetchApp.fetch(uri + path, options)).result;
}

function AllCommandGet(cmdpath, apikey, apisecret) {
    return CommandGet(cmdpath, apikey, apisecret, null)
}
