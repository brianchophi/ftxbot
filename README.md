# ftxbot

## 簡述
提供一自動化交易機器人,使用FTX的API串接

## 資料夾結構
```text
FTXBOT/
 ├── ftxexchange/   # FTX-API
 ├── gridsbot/      # 網格交易機器人
 ├── line/          # line notify
 └── README.md      # 說明文件
```


## 網格交易機器人

### 網格機器人參數
```yaml
botName: "tester"
apikey: ""
apisecret: ""
subaccount_name: "ALi"
baseCurrency: "FTT"
quoteCurrency: "USD"
grid_pct: 0.02
grid_volume: 0.1
grid_number: 2
post_only: true
lineToken: ""
```
### 進入點

```shell
cd ftxbot/gridsbot/
go run ./main.go
```

## FTX API 文檔
https://docs.ftx.com/?python#overview


