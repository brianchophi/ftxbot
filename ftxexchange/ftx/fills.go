package ftx

import (
	"ftxbot/ftxexchange/ftx/structs"
	"log"
	"strconv"
)

func (client *FtxClient) GetFills(market string, startTime int64) (structs.Fills, error) {
	var fills structs.Fills
	resp, err := client._get(
		"fills?market="+market+
			"&start_time="+strconv.FormatInt(startTime, 10)+"&limit=10000",
		[]byte(""))
	if err != nil {
		log.Printf("Error GetFills", err)
		return fills, err
	}
	err = _processResponse(resp, &fills)
	return fills, err
}

func (client *FtxClient) GetFillsWithTime(market string, startTime int64, endTime int64) (structs.Fills, error) {
	var fills structs.Fills
	resp, err := client._get(
		"fills?market="+market+
			"&start_time="+strconv.FormatInt(startTime, 10)+"&end_time="+strconv.FormatInt(endTime, 10),
		[]byte(""))
	if err != nil {
		log.Printf("Error GetFills", err)
		return fills, err
	}
	err = _processResponse(resp, &fills)
	return fills, err
}

func (client *FtxClient) GetLastFill(market string) (structs.Fills, error) {
	var fills structs.Fills
	resp, err := client._get(
		"fills?market="+market+"&limit=1",
		[]byte(""))
	if err != nil {
		log.Printf("Error GetLastFill", err)
		return fills, err
	}
	err = _processResponse(resp, &fills)
	return fills, err
}
