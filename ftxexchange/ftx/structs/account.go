package structs

type Positions struct {
	Success bool              `json:"success"`
	Result  []PositionsResult `json:"result"`
}

type PositionsResult struct {
	Cost                         float64 `json:"cost"`
	CumulativeBuySize            float64 `json:"cumulativeBuySize"`
	CumulativeSellSize           float64 `json:"cumulativeSellSize"`
	EntryPrice                   float64 `json:"entryPrice"`
	EstimatedLiquidationPrice    float64 `json:"estimatedLiquidationPrice"`
	Future                       string  `json:"future"`
	InitialMarginRequirement     float64 `json:"initialMarginRequirement"`
	LongOrderSize                float64 `json:"longOrderSize"`
	MaintenanceMarginRequirement float64 `json:"maintenanceMarginRequirement"`
	NetSize                      float64 `json:"netSize"`
	OpenSize                     float64 `json:"openSize"`
	RealizedPnl                  float64 `json:"realizedPnl"`
	RecentAverageOpenPrice       float64 `json:"recentAverageOpenPrice"`
	RecentBreakEvenPrice         float64 `json:"recentBreakEvenPrice"`
	RecentPnl                    float64 `json:"recentPnl"`
	ShortOrderSize               float64 `json:"shortOrderSize"`
	Side                         string  `json:"side"`
	Size                         float64 `json:"size"`
	UnrealizedPnl                float64 `json:"unrealizedPnl"`
	CollateralUsed               float64 `json:"collateralUsed"`
}

type Account struct {
	Success bool          `json:"success"`
	Result  AccountResult `json:"result"`
}

type AccountResult struct {
	BackstopProvider             bool              `json:"backstopProvider"`
	Collateral                   float64           `json:"collateral"`
	FreeCollateral               float64           `json:"freeCollateral"`
	InitialMarginRequirement     float64           `json:"initialMarginRequirement"`
	Leverage                     float64           `json:"leverage"`
	Liquidating                  bool              `json:"liquidating"`
	MaintenanceMarginRequirement float64           `json:"maintenanceMarginRequirement"`
	MakerFee                     float64           `json:"makerFee"`
	MarginFraction               float64           `json:"marginFraction"`
	OpenMarginFraction           float64           `json:"openMarginFraction"`
	TakerFee                     float64           `json:"takerFee"`
	TotalAccountValue            float64           `json:"totalAccountValue"`
	TotalPositionSize            float64           `json:"totalPositionSize"`
	Username                     string            `json:"username"`
	Positions                    []PositionsResult `json:"positions"`
}
