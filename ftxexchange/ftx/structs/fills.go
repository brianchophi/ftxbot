package structs

import (
	"time"
)

type Fills struct {
	Success bool   `json:"success"`
	Result  []Fill `json:"result"`
}

type Fill struct {
	Fee           float64   `josn:"fee"`
	FeeCurrency   string    `josn:"feeCurrency"`
	FeeRate       float64   `josn:"feeRate"`
	Future        string    `josn:"future"`
	Id            int64     `josn:"id"`
	Liquidity     string    `josn:"liquidity"`
	Market        string    `josn:"market"`
	BaseCurrency  string    `josn:"baseCurrency"`
	QuoteCurrency string    `josn:"quoteCurrency"`
	OrderId       int64     `josn:"orderId"`
	TradeId       int64     `josn:"tradeId"`
	Price         float64   `josn:"price"`
	Side          string    `josn:"side"`
	Size          float64   `josn:"size"`
	Time          time.Time `josn:"time"`
	Type          string    `josn:"type"`
}
