package structs

import "time"

type Future struct {
	Success bool `json:"success"`
	Result  struct {
		Ask            float64   `json:"ask"`
		Bid            float64   `json:"bid"`
		Change1h       float64   `json:"change1h"`
		Change24h      float64   `json:"change24h"`
		Description    string    `json:"description"`
		Enabled        bool      `json:"enabled"`
		Expired        bool      `json:"expired"`
		Expiry         time.Time `json:"expiry"`
		Index          float64   `json:"index"`
		Last           float64   `json:"last"`
		LowerBound     float64   `json:"lowerBound"`
		Mark           float64   `json:"mark"`
		Name           string    `json:"name"`
		Perpetual      bool      `json:"perpetual"`
		PostOnly       bool      `json:"postOnly"`
		PriceIncrement float64   `json:"priceIncrement"`
		SizeIncrement  float64   `json:"sizeIncrement"`
		Underlying     string    `json:"underlying"`
		UpperBound     float64   `json:"upperBound"`
		Type           string    `json:"type"`
	} `json:"result"`
}
